package com.vlx.wifiscanrpg

class PlayerStats() {
    private var totalXP: Int = 0
    private var level: Int = 0
    private val levelLimit: Int = 100

    init {
        addXP(0)
    }

    fun addXP(xp: Int) {
        totalXP += xp
        calcLevel(totalXP)
    }

    fun calcLevel(xp: Int) {
        level = xp / levelLimit + 1
    }

    fun getLevel(): Int {
        return level
    }

    fun getXP(): Int {
        return totalXP
    }

    fun calcProgress(): Int {
        return totalXP - (level - 1) * levelLimit
    }

}