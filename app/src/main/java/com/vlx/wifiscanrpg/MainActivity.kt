package com.vlx.wifiscanrpg

import android.Manifest
import android.app.AlertDialog
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.net.wifi.WifiManager
import android.os.Bundle
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.ListView
import android.widget.ProgressBar
import android.widget.TextView
import androidx.activity.ComponentActivity
import androidx.core.app.ActivityCompat
import com.example.wifiscanrpg.R


@Suppress("DEPRECATION")
class MainActivity : ComponentActivity() {

    val PREFS_NAME = "Save Data"

    lateinit var wifiResults: ListView
    lateinit var wifiManager: WifiManager
    lateinit var scanWifiBtn: Button
    lateinit var playerStats: PlayerStats
    lateinit var progressBar: ProgressBar
    lateinit var playerLevel: TextView
    lateinit var playerXP: TextView
    lateinit var enemyList: ArrayList<String>
    var defeatedEnemies: Int = 0

    private val wifiScanReceiver = object : BroadcastReceiver() {

        override fun onReceive(context: Context, intent: Intent) {
            checkLocationPermission()
            unregisterReceiver(this)

            val wifiList = wifiManager.scanResults
            val listItems = ArrayList<String>()

            val saveProg = getSharedPreferences(PREFS_NAME, MODE_PRIVATE)
            val editor = saveProg.edit()

            for (i in 0 until wifiList.size) {
                val recipe = wifiList[i]
                if (recipe.SSID != "") {
                    listItems.add(recipe.SSID)
                    if (recipe.SSID !in enemyList){
                        playerStats.addXP(5)
                        enemyList.add(recipe.SSID)
                        defeatedEnemies++
                        editor.putString(""+defeatedEnemies, recipe.SSID)
                    }
                }
            }

            editor.putInt("xp", playerStats.getXP())
            editor.putInt("defeatedEnemies", defeatedEnemies)
            editor.apply()

            progressBar.progress = playerStats.calcProgress()
            playerLevel.text = "" + playerStats.getLevel()
            playerXP.text = "" + playerStats.getXP() + "/" + playerStats.getLevel() * 100

            val adapter =
                ArrayAdapter(applicationContext, android.R.layout.simple_list_item_1, listItems)
            wifiResults.adapter = adapter
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        wifiManager = this.getSystemService(Context.WIFI_SERVICE) as WifiManager
        wifiResults = findViewById(R.id.wifires)
        scanWifiBtn = findViewById(R.id.scanwifibtn)
        progressBar = findViewById(R.id.xpbar)
        playerLevel = findViewById(R.id.playerlevel)
        playerXP = findViewById(R.id.playerxp)

        checkLocationPermission()

        val saveProg = getSharedPreferences(PREFS_NAME, MODE_PRIVATE)

        playerStats = PlayerStats()
        playerStats.addXP(saveProg.getInt("xp", 0))

        defeatedEnemies = saveProg.getInt("defeatedEnemies", 0)
        enemyList = ArrayList()
        for (i in 1..defeatedEnemies){
            enemyList.add(saveProg.getString(""+i,"").toString())
        }

        progressBar.progress = playerStats.calcProgress()
        playerLevel.text = "" + playerStats.getLevel()
        playerXP.text = "" + playerStats.getXP() + "/" + playerStats.getLevel() * 100

        scanWifiBtn.setOnClickListener {
            val intentFilter = IntentFilter()
            intentFilter.addAction(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION)
            registerReceiver(wifiScanReceiver, intentFilter)
            wifiManager.startScan()
        }
    }

    private fun checkLocationPermission() {
        if (ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(
                    this,
                    Manifest.permission.ACCESS_FINE_LOCATION
                )
            ) {
                AlertDialog.Builder(this)
                    .setTitle("Location Permission Needed")
                    .setMessage("This app needs the Location permission, please accept to use location functionality")
                    .setPositiveButton("OK") { _, _ ->
                        ActivityCompat.requestPermissions(
                            this,
                            arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                            99
                        )
                    }.create().show()
            } else {
                ActivityCompat.requestPermissions(
                    this,
                    arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                    99
                )
            }
        }
    }
}